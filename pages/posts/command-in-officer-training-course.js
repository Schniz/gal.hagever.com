import React from "react";
import Page from "../../src/Page";
import HashlamaPost from "../../src/HashlamaPost";

export default () => (
  <Page
    title="לפקד בהשלמה הטכנולוגית / גל שלזינגר"
    url="https://gal.hagever.com/posts/command-in-officer-training-course"
    image="https://gal.hagever.com/static/hashlama_post.jpg"
    description="בשבוע האחרון סיימתי את התפקיד הטוב ביותר שהיה לי בשירות - לפקד ולהדריך בהשלמה הטכנולוגית של חיל התקשוב - ולמדתי מלא על עצמי."
  >
    <HashlamaPost />
  </Page>
);
