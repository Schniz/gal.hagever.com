import React from "react";
import Page from "../src/Page";
import PostListPage from "../src/PostListPage";

const { posts } = require("../contents");
const setDates = xs =>
  xs.map(x => ({ ...x, posted_at: new Date(x.posted_at) }));
const descending = prop => (a, b) => b[prop] - a[prop];
const postsWithDates = setDates(posts).sort(descending("posted_at"));

export default class IndexPage extends React.Component {
  render() {
    return (
      <Page
        title="Gal Schlezinger"
        image="https://gal.hagever.com/static/image.jpg"
        description="developer by day; rocker by night"
        url="https://gal.hagever.com"
      >
        <PostListPage posts={postsWithDates} />
      </Page>
    );
  }
}
