# לפקד בהשלמה הטכנולוגית
> נכתב ב16 במאי, 2017

בשבוע האחרון סיימתי את התפקיד הטוב ביותר שהיה לי בשירות - לפקד ולהדריך בהשלמה הטכנולוגית של חיל התקשוב - ולמדתי מלא על עצמי.

“צוערים יקרים, או יותר נכון - קצינים יקרים, קחו הפיקוד!”. אלה המילים האחרונות שאמרתי בנאום הסיום של ההשלמה הטכנולוגית. בנאום התחלתי בלדבר על המסלול שעברו, שמלווה במלא קשיים. רובם, אגב, מנטליים בכלל. המשכתי בלדבר על הציפיות שלנו, כסגל, מהם. הציפיות שלנו לאו דווקא טכנולוגיות - אני מצפה שהקצינים הטכנולוגים יהיו הסמן הימני, חוד החנית, של החברה הישראלית. זו לא זכותנו, אלא חובתנו, בסופו של דבר, לבנות מדינה טובה יותר.

לפקד בהשלמה על 8 צוערים בצוות שלי, ועוד 76 צוערים בצוותים אחרים, לימד אותי הרבה. תמיד החשבתי את עצמי כבן אדם ציני - ופה, הייתי חייב לשחרר מהציניות. הייתי חייב לדבר עם אנשים בגובה העיניים על החששות, על התהיות, אבל בעיקר - לשתף מעצמי על החששות, התהיות והבעיות שהיו לי בתפקידים. כל חיי הייתי הבחור הציני שלא מתייחס לשום דבר ברצינות. כמפקד בצבא, גם כן - לא התייחסתי ברצינות להרבה דברים ולא עשיתי שום דבר חינוכי. בכלל, למרות שהרבה ממשפחתי מגיעים מהמקום הזה - ללכת בעצמי למקום של המחנך - נראה לי מאוד מוזר ולא טבעי. בסופו של יום, אני איש מחשבים.

## תתעלמו מדעות של אחרים
כל פעם שסיפרתי שאני מפק”ץ בהשלמה, התגובה שקיבלתי זה פרצוף עקום. “מסכן,” כולם אמרו לי. כשאמרתי שהתנדבתי, הגיבו בפליאה. תפקיד המפק”ץ נתפש כתפקיד גרוע. משלוש סיבות ברורות:

1. עוברים לדרום לחצי שנה עד שנה
2. משרתים בסיס סגור וסוגרים מלא שבתות
3. לא נוגעים טכנית בפיתוח

אין ספק שזה נכון. עוברים לדרום, לחצי שנה עד שנה, וזה מאתגר. זה מאתגר, כי כל החיים שלכם נשארים במרכז (במידה ואתם מתגוררים במרכז). החברה, החברים, המשפחה - הכל. גם אם אתם דרומיים, אז אתם משרתים עכשיו בבסיס סגור ואתם לא מגיעים הביתה כל פעם. מקובל לקחת אפטרים לפעמים, אבל זה לא העניין - בדרך כלל אתם בבסיס. אני, אגב, חושב שהדברים האלה מעניינים. אמנם לא הייתי בזוגיות בזמן ההשלמה, אבל הזמן הזה, כשאתה רחוק מהכל, נותן לך את הזמן לחשוב. הוא גורם לכל פעם שאתה בבית להיות פרודוקטיבית מאוד. לפתע, בגיל 25, אתה מרגיש שוב משמעות. הנקודה האחרונה גם היא נכונה - לא נוגעים טכנית בפיתוח.

האם מתמקצעים פחות? בתכנות כן, אבל בדברים אחרים ממש לא. יחסי אנוש זו יכולת שאנחנו שוכחים ממנה בתור אנשים טכנולוגיים הרבה פעמים. הובלת משימה והנעת אנשים, גם, יכולות שאנחנו מזניחים. העולם לא סובב סביב תכנות ופיתוח. ואם כבר מדברים על פיתוח, פיתוח האנשים שלכם - גם את זה אנחנו מזניחים. כשהגעתי להשלמה, הייתי האדם הכי פחות מקצועי בנושאים האלה - וזו פעם ראשונה שחוויתי איך זה להיות לא מקצועי. לפתע, הידע הטכני שלי לא באמת עזר אלא הייתי צריך לעבוד על דברים אחרים. זה בדיוק מה שגרם לי להתפתח, מה שגרם לי ללמוד ולהנות מהתקופה הזו.

התהייה האם בכלל מתאים לי ללכת להיות מפקד בהשלמה התחילה עוד כשאני הייתי צוער בהשלמה. מאז ומתמיד, תמיד אמרו שאני “הילד המוזר” וכולם, כולל החברים הקרובים אליי ביותר, הופתעו ש”אשכרה לא זרקו אותי מהקצונה”. למה? כי אני אומר מה שאני חושב, והרבה פעמים זה לא מתכנס עם החשיבה הרגילה של אנשים. המפקד שלי בהשלמה, כשהייתי צוער, אמר שהוא חושב שיתאים לי להיות מפק”ץ וגם אז זה נראה לי מוזר. אני ממש לא דומה לכל מפק”ץ אחר שהיה, מכל מיני טענות שעלו לי במהלך השנים:

1. אני לא איש ההדרכה הטוב ביותר
2. אני לא מעוניין בקריירה צבאית, ומפק”ץ זה תפקיד קידום.
3. אני לא בן אדם עם מרחק פיקודי משמעותי
4. אני לא מספיק מחובר רגשית בשביל לפקד בקורס
5. אני לא מנהל הפרויקט הכי טוב
6. אני יותר תכניתן מאשר מפקד

כל הטענות האלה לא רלוונטיות או מדויקות, כמובן. כל מה שהן, זה סמל לחשש שלי מלשנות את המסגרת שלי; מסגרת ממש נוחה בה אני רגיל להתמודד עם החסרונות שלי, ויודע להבליט את התכונות הטובות בי. איך בדיוק נפתח את עצמנו אם נוח לנו עד כדי כך?

### אני לא איש ההדרכה הטוב ביותר
הכל בסדר. כמפק”ץ בהשלמה, לא הייתי צריך להיות איש ההדרכה הטוב ביותר. אני עדיין חושב שזה מצויין שממש רציתי להיות אחד טוב - כי ככה יכולתי לעבור תהליך גם בנושא הזה ודרשתי להעביר שיעורים מקצועיים. אבל ברמת התפקיד - זה לא *באמת* נדרש. לעומת שיעורים מקצועיים בהשלמה, שבדרך כלל עוברים על ידי צוערים, השיעורים שהמפק”ץ מעביר - כמו שיעורי מנהיגות וזמנים צוותיים, מטרתם, מבחינתי לפחות, היא לא להעביר שיעור, אלא לבצע שיח אמיתי וכנה על דברים שמעניינים אותך ולדעתך חשוב לדבר עליהם כמפקדים ומנהיגים טכנולוגיים. הצוערים בצוות שלי לא פעם אחת אמרו שהם חייבים לדעת מה הנושא של השיעור, כי התחלנו לדבר על הארי פוטר ושטויות כאלה. הסברתי להם שאנחנו מדברים על כל מיני דברים, ובסוף, אנחנו תמיד נוגעים ברציונאל השיעור. המטרה היא לעודד שיח פתוח ואמיתי, ואם ככה אנחנו משיגים את זה, אני מרוצה.

### אני לא מעוניין בקריירה צבאית, ומפק”ץ זה תפקיד קידום
אני משתחרר ממש בקרוב. כשהלכתי למפקד מצפ”ן ודרשתי ללכת להשלמה, אמרתי שאני רוצה לעשות מחזור אחד ואז להשתחרר אחריו, כלומר, שאני לא מאריך יותר את השירות. מפקד מצפ”ן לא היה מופתע, ובראייה לאחור, גם אני לא הייתי צריך להיות. תפקיד המפק”ץ הוא ללא ספק תפקיד קידום, אבל לפני שהוא קידום בדרגות וקידום בתפקידים, הוא תפקיד קידום אישי בשביל המפק”ץ. הוא מפתח בכם דברים שלא חשבתם שתוכלו לפתח ביחידות הטכנולוגיות, ויצאו ממזמן מהאופנה מבחינתנו. פיקוד, הנהגה, ניהול, ואפילו - קבלה עצמית. תוך כדי התפקיד, אתם תבינו גם למה הוא נחשב כתפקיד קידום.

### אני לא בן אדם עם מרחק פיקודי משמעותי
כל מי שהיה איתי בשירות יודע שאני מדבר הרבה יותר מדיי. אני ממש פתוח ואני לא מסתיר הרבה דברים. כנראה זה לא מפתיע שהייתי ראש צוות קהילת קוד פתוח, אבל לא נכנס לזה. העניין הוא שזה בסדר, ובכלל, קורסים כמו השלמת הקצונה לא צריכים להיות עם “מרחק פיקודי משמעותי”. מה שצריך להיות זה כבוד, כבוד הדדי, וההבנה של כל אחד איפה הוא נמצא כרגע. לצוערים שלי חשפתי הרבה פרטים מהחיים שלי, מהסיבות שגרמו לי ללכת להשלמה והבעיות האישיות שהיו לי בחיים. אני לא חושב שזה גרם להם להעריך אותי פחות - אלא יותר. אבל יכול להיות שאני נרקיסיסט ולא מחובר למציאות. בעצם, מה הסיכוי?

אנשים אוהבים פתיחות. במיוחד אחרי שאנשים מגיעים מבה”ד 1, שם הכבוד מגיע כדרישה, לפעמים מגוחכת אפילו, זה בדיוק המקום להיות קצת פחות נוקשים ולדבר באמת ובתכלס. בסוף, הדבר שאנחנו הכי רוצים זה שיעברו תהליך, ואם הם יהיו ציניים לעניין - זה לא יעבוד. הפתיחות והכנות שלנו זה הבסיס  לזה. בכלל, אני חושב שהנקודה הזו גרמה לצוערים מסוימים להפתח אליי יותר מלמפק”צים אחרים. *ברור שגם ההפך הוא הנכון*, ולכן זה מעולה שיש את המגוון.

כמובן, שעדיין נצטרך להיות מערכתיים. זו גם ציפייה שלנו מהצוערים. *למדר אנשים זה לא נורא*. יש דברים שאנשים לא צריכים לדעת, וכל עוד יש לנו את הרציונאל למה הם לא צריכים לדעת, זה תופס. לפעמים נסתכל בכוונה על רבע הכוס המלאה ולא על שלושת-רבעי הכוס הריקה - רק כי להסתכל על שלושת-רבעי הכוס הריקה לא תעזור לפקודים או לעמיתים שלנו.

### אני לא מספיק מחובר רגשית כדי לפקד בקורס
בעבודה השוטפת ביחידת פיתוח, יצא לי להתעלם מהרבה דברים שקרו, ולקחת אותם לא ברצינות. כשהגעתי להשלמה, והתחלתי לדבר על זה עם הצוערים שלי, הבנתי שלא הייתי כזה גרוע בזה. במהלך השרות ביחידת הפיתוח הייתי עושה שיחות ללא הפסקה עם הפקודים שלי ועם קולגות, על החיים, על הפסיכולוג, על העבודה - על הכל. אמנם לא הייתי מהמחבקים, אבל הייתי מקשיב. לפעמים - מייעץ. רוב הזמן, זה מה שצריך.

גם בהשלמה יצא לי לעשות שיחות כאלה, גם, לא הרבה, אבל שיחות שמסבירות למה מה שאנחנו עושים חשוב ולמה הקושי המנטאלי שאנחנו עוברים הוא מצוין. על זה בדיוק מדברים כשמדברים על “תהליך חניכה”, לדעתי. הצוערים שעשיתי את זה איתם, *נחרטו אצלי בלב*. אני מקווה שזה היה גם הפוך אבל רק הזמן יוכל לספר לנו.

### אני לא מנהל הפרויקט הכי טוב
זה לא שאני לא מנהל פרויקט טוב. אלא שהשיטות שלי, שמבוססות על חברות כמו גיטהאב וגיטלאב, ומתמקדות ב”פשוט לעשות דברים”, לא מתקבלים הרבה פעמים בארגון. למה הם לא מתקבלים? כי אנשים אוהבים את המטה-דאטה שלהם. מה שמדהים הוא שזה בדיוק המקום להסביר על השיטות שלי, לראשי צוותים חדשים שיגיעו לעבוד על פרויקטים. לדבר עם אנשים על פתיחות, על משימות פשוטות ועל עבודה מנהלתית כמה שיותר מצומצמת כדי לפתח כמה שיותר.

### אני יותר תכניתן מאשר מפקד
זה משפט שאמרתי על עצמי כל כך הרבה פעמים. זה משפט שאמרו עליי כל כך הרבה פעמים. תמיד תפסו אותי בתור הדמות המקצועית בכל מקום שהגעתי, והטענה היא שאנשים הולכים אחריי בגלל זה. לא בגלל שום דבר אחר. בהשלמה, גיליתי אחרת. כמו שאמרתי מקודם, בהשלמה אני נחשב פחות מקצועי כי אני פחות מסודר ומאורגן, אני אדם פחות רגיש וכו’. אין פה עניין מקצועי של תוכנה בכלל. אבל עדיין, הסברתי לצוערים ואפילו למפק”צים המקבילים את אג’נדת הפיקוד שלי - ואני לא חושב שנפלתי בזה בכלל.

קיבלתי הרבה ביקורת מהמפק”צים האחרים על רמת הפתיחות שלי עם הצוערים, עם העובדה שאני מדבר הרבה שטויות, שאני צוחק, ושאין לי מרחק פיקודי משמעותי איתם, כמו שאמרתי מקודם. הייתי מסכים שזו בעיה, אם לא הייתי יודע להתמודד עם התוצאות בצורה הולמת. בין אם זה לדבר עם צוער על המצב שלו בהשלמה, ולבשר לו שהוא לא עומד בסטנדרטים שאני דורש, ובין אם זה להסביר לצוער למה הוא קיבל ריתוק. זה לא סתם להסביר למה הוא קיבל ריתוק, אלא להסביר לו מה הוא צריך לקחת מזה, למה זו אופציה ללמוד, למה העונש הוא לטובתו.

לחנך, להסביר, לפתח את האנשים תחתיך. אלה הם אתגרי הפיקוד. ומסתבר שהתמודדתי איתם מעולה.


## “אסור לך להיות חרא”
בהכנת הסגל, דוסו עבד איתי על זה שאני צריך לקבל שלאנשים אחרים קשה. אסור לי להיות מרושע ולענות בציניות להכל. זה היה החשש הראשון שהיה לי אל מול הצוערים העתידיים שלי. חששתי, שאם אני אהיה מרושע מדיי - הם לא יעברו תהליך משמעותי. הדבר הכי חשוב בכל ההשלמה הזו, זה שהם יעברו תהליך. חששתי מלהוריד את שכבת הציניות שלי, שאני כל כך רגיל אליה. למען האמת, לא ידעתי בכלל איך אני אתנהג בלי אותה שכבה.

למדתי שזה בסדר להיות לא ציני, וזה אפילו מחבר אותך הרבה יותר לאנשים. כן, להיות ציני ומצחיק גורם לך להיות מגניב - אבל הקשר יהיה פחות עמוק. בשלושה חודשים התחברתי לאנשים שבדרך כלל לא הייתי מתחבר, בגלל מנגנוני ההגנה שלי.

## את/ה חושב/ת שאת/ה לא מתאים/ה להיות מפק”ץ/ית? טעות.
זו האחריות שלנו. זו האחריות שלנו, המסוגלים לכך, ללכת לפקד ולהדריך במקומות שמכשירים את המנהיגים הבאים שלנו. הטעם הייחודי שכל אחד מהמפק”צים מוסיף לפויקה של הקורס משנה אותו לחלוטין. בלי דוסו, ההשלמה הייתה הרבה פחות מתמקדת בנושא החינוכי. בלי ניב, ההשלמה הייתה הרבה פחות “צבאית”, רשמית ומופנית כלפי התכנים המבצעיים-צבאיים. בלעדיי, ההשלמה הייתה פחות מודרנית, מקצועית - ולדעתי, היא הייתה גם פחות פתוחה, אישית ו”תכלס”ית.

את/ה חושב/ת שאת/ה לא מתאים/ה להיות מפק”ץ/ית? טעות. זה לא שהתפקיד מגדיר אותך, אלא אתה מגדיר את התפקיד. אני חושב שבסופו של דבר, עם כמה אבסורדי שזה ישמע, תפקיד המפק”ץ היה התפקיד שהכי הרבה ניצל את היכולות שלי. הגעתי למקום בו כל דבר אפשר לפתוח, כל דבר אפשר לשנות. תבינו, מפקדי העתיד יכולים להיות בידיים שלכם. עתיד המדינה, ואני לא מדבר רק על בטחון המדינה, אלא על התרבות, החינוך והדמות הישראלית כולה, האחריות הזו - יכולה להיות בידיים שלכם.

וכל מה שצריך לעשות זה לבקש אותה.
