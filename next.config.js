module.exports = {
  exportPathMap: function() {
    return {
      '/': { page: '/' },
      '/posts/command-in-officer-training-course': { page: '/posts/command-in-officer-training-course' },
    }
  }
}
