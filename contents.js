module.exports = {
  posts: [{
    type: "blog",
    id: "leaving-the-comfort-zone",
    title: "Leaving The Comfort Zone",
    posted_at: "2016-12-21",
    link: "https://medium.com/@galstar/leaving-the-comfort-zone-edaf10f9e536",
    description: "I’m leaving my convenient development infrastructure team leading role in favor of being a commander in the technological officers course.",
  }, {
    type: "blog",
    id: "regression",
    title: "Regression",
    posted_at: "2016-10-06",
    link: "https://medium.com/@galstar/regression-54cf8153eabd",
    description: "Running back and forth between development environments. Time traveling my development decisions.",
  }, {
    type: "blog",
    id: "composable-sql-in-javascript",
    title: "Composable SQL in JavaScript",
    posted_at: "2016-06-26",
    link: "https://medium.com/@galstar/composable-sql-in-javascript-db51d9cae017",
    description: "Using SQL queries in conjunction with Functors to create reusable querying systems. How to build a simple, composable SQL query system in JS.",
  }, {
    type: "blog",
    id: "being-in-a-band-to-better-software-developer",
    title: "Being in a band made me a better software developer. vice versa.",
    posted_at: "2016-12-29",
    link: "https://medium.com/@galstar/being-in-a-band-made-me-a-better-software-developer-vice-versa-5161ebd4204e",
    description: "I’ve been playing guitar for almost 12 years now, and it affected my perspective about software development, commandership and team leading.",
  }, {
    type: "blog",
    id: "i-failded-to-deliver",
    title: "I failed to deliver.",
    posted_at: "2017-01-18",
    link: "https://medium.com/@galstar/i-failed-to-deliver-f9c0385a6a4#.8p19c8v00",
    description: "Over the last 3 years, I’ve built some inter-organization communication tools that failed. They all failed for the same reasons.",
  }, {
    type: "blog",
    id: "versatility-is-your-enemy",
    title: "Versatility is your enemy",
    posted_at: "2017-01-25",
    link: "https://medium.com/@galstar/versatility-is-your-enemy-9e806500b812",
    description: "The larger set of options I get, the less work I can actually do.",
  }, {
    type: "speakerdeck",
    id: "declarative-heaven-talk",
    title: "Declarative Heaven with and without React.js (slides)",
    posted_at: "2017-03-30",
    link: "https://speakerdeck.com/schniz/declarative-heaven-with-and-without-react-dot-js",
    thumbnail: "https://speakerd.s3.amazonaws.com/presentations/e9724a4739ae456d8270387d8c05feb3/slide_5.jpg",
    description: "Functional programming and other weird tips for writing readable, testable and declarative code in JavaScript and specifically React.",
    place: "React Israel Meetup Group",
    categories: ["React.js", "JavaScript", "Declarative Programming", "Functional Programming"],
  }, {
    type: "blog",
    id: "ifttt-ideas-for-software-development",
    title: "IFTTT Ideas for Software Development",
    posted_at: "2017-04-12",
    link: "https://medium.com/@galstar/ifttt-ideas-for-software-development-fcae4f1f580e",
    description: "I use multiple types of software every day. I’d like to connect them to maximize productivity and organization.",
  }, {
    type: "blog",
    id: "sign-up-and-sign-in-forms-could-and-should-be-the-same",
    title: "“Sign up” and “Sign in” forms could (and should) be the same",
    posted_at: "2017-05-05",
    link: "https://medium.com/@galstar/sign-up-and-sign-in-forms-could-and-should-be-the-same-19835c1e4fc7",
    description: "After reading Omri Lachman’s article about hidden “log in” buttons, I understood my frustration with recent web apps.",
  }, {
    type: "blog",
    id: "command-in-officer-training-course",
    title: "לפקד בהשלמה הטכנולוגית (Hebrew)",
    posted_at: "2017-05-16",
    link: "/posts/command-in-officer-training-course",
    permalink: 'https://gal.hagever.com/posts/command-in-officer-training-course',
    inside_link: true,
    description: 'בשבוע האחרון סיימתי את התפקיד הטוב ביותר שהיה לי בשירות - לפקד ולהדריך בהשלמה הטכנולוגית של חיל התקשוב - ולמדתי מלא על עצמי.',
  }, {
    type: "blog",
    id: "fun-with-functors-and-reasonml",
    title: "Fun with Functors and ReasonML",
    posted_at: "2018-03-25",
    link: "https://hackernoon.com/fun-with-functors-and-reasonml-8bb59b243e9c",
    description: "Reasonable Functional Programming Part 1: A short introduction to ReasonML types and functor types from a beginner.",
  }, {
    type: "blog",
    id: "the-programming-language-im-looking-for",
    title: "The Programming Language I’m Looking For",
    description: "I love programming languages. Every one of them has this catchy thing that I wished the others had. Lately, I’ve been distracted by the question of which language would I use if I would start a new side project.",
    posted_at: "2018-09-24",
    link: "https://hackernoon.com/the-programming-language-im-looking-for-948d93f7a396"
  }, {
    type: "blog",
    id: "rs256-in-reasonml",
    title: "RS256 in ReasonML/OCaml",
    description: "It took me a decent amount of time figuring out how to sign messages for a ReasonML GitHub app. Here are my findings.",
    posted_at: "2018-12-04",
    link: "https://hackernoon.com/rs256-in-ocaml-reasonml-9ae579b9420a"
  }]
}
