import React from "react";
import styled from "styled-components";
import Post from "./Post";
import MediaIsMobile from "./MediaIsMobile";

const PaddedList = styled.ul`
  padding: 0 1em;

  ${p =>
    !p.mobile &&
    `
    &:hover article {
      opacity: 0.4;
    }
  `};
`;

export const PostList = ({ posts }) => (
  <MediaIsMobile>
    {isMobile => (
      <PaddedList mobile={isMobile}>
        {posts.map(post => <Post {...post} key={post.id} />)}
      </PaddedList>
    )}
  </MediaIsMobile>
);

export default PostList;
