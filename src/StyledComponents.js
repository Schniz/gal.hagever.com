import styled, { injectGlobal } from "styled-components";

export const WeirdHeader = styled.h1`
  color: #555;
  font-family: Courier New, Courier;
  font-size: 1em;
  font-weight: 100;
  text-align: center;
  padding-top: 0.4em;
`;

export const CenterContainer = styled.div`
  max-width: ${p => p.width || 900}px;
  margin: auto;
`;

export const Parallaxed = styled.div`
  margin-top: 10em;
  background-color: #fff;
`;

injectGlobal`
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
  }
`;
