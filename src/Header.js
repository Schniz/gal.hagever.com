import React from "react";
import styled from "styled-components";

const HeaderWrapper = styled.header`
  color: red;
  text-align: center;
  display: block;
  background-image: url("/static/header.jpg");
  background-size: cover;
  background-position: center;
  width: 100vw;
  top: 0;
  position: fixed;
  z-index: -1;
`;

const NameWrapper = styled.h1`
  background-color: rgba(0, 0, 0, 0.3);
  font-family: Arial;
  color: white;
  font-weight: 100;
  padding: 1em;
  margin: 0;
  font-size: ${p => p.size}em;
  text-shadow: 0 0 3px rgba(0, 0, 0, 1);

  &:hover > .ignore {
    opacity: 0;
  }

  & > .ignore {
    opacity: 1;
    transition: all 0.2s ease;
  }
`;

export default ({ opacity }) => (
  <HeaderWrapper>
    <NameWrapper opacity={opacity} size={2}>
      <span className="ignore">GAL </span>
      SCHLEZ
      <span className="ignore">INGER</span>
    </NameWrapper>
    <NameWrapper opacity={opacity} size={1}>
      <code>developer by day; rocker by night</code>
    </NameWrapper>
  </HeaderWrapper>
);
