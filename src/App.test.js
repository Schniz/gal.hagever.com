import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

let mockIsMobile = false;
jest.mock("./MediaIsMobile", () => ({ children }) => children(mockIsMobile));
jest.mock("../contents", () => ({ posts: [] }));

[true, false].forEach(isMobile => {
  it(`renders without crashing when mobile is ${isMobile}`, () => {
    mockIsMobile = isMobile;
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
  });
});
