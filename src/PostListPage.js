import React from "react";
import { WeirdHeader, CenterContainer } from "./StyledComponents";
import PostList from "./PostList";
import ScrollToTopOnMount from "./ScrollToTopOnMount";

export const PostListPage = ({ posts }) => (
  <ScrollToTopOnMount>
    <CenterContainer>
      <WeirdHeader>~~~weird thoughts below~~~</WeirdHeader>
      <PostList posts={posts} />
    </CenterContainer>
  </ScrollToTopOnMount>
);

export default PostListPage;
