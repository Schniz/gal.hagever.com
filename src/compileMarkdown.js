import React from "react";
import marksy from "marksy";

const marked = marksy();

export const compile = (elements, text) =>
  marked(text).tree.map(e => {
    const Component = elements[e.type];
    return !Component ? e : <Component key={e.key} {...e.props} />;
  });

export default compile;
