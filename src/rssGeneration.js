const RSS = require("rss");
const { posts } = require("../contents");
const fs = require("fs");
const path = require("path");

const setDates = xs =>
  xs.map(x => ({ ...x, posted_at: new Date(x.posted_at) }));
const descending = prop => (a, b) => b[prop] - a[prop];
const postsWithDates = setDates(posts).sort(descending("posted_at"));

const feed = new RSS({
  title: "Gal Schlezinger's thoughts",
  description: "developer by day; rocker by night",
  feed_url: "https://gal.hagever.com/rss.xml",
  site_url: "https://gal.hagever.com",
  generator: "https://www.npmjs.com/package/rss",
  image_url: "https://gal.hagever.com/static/image.jpg",
  managingEditor: "Gal Schlezinger",
  language: "English / Hebrew",
  categories: ["Technology", "Code", "JavaScript", "Ruby", "Development"],
  pubDate: posts[0].posted_at
});

postsWithDates.slice(0, 20).forEach(post => {
  feed.item({
    title: post.title,
    description: post.description,
    url: post.permalink || post.link,
    guid: post.id,
    categories: post.categories,
    author: "Gal Schlezinger",
    date: post.posted_at
  });
});

const xml = feed.xml({ indent: true });

fs.writeFileSync(path.resolve(__dirname, "../out/rss.xml"), xml, "utf-8");
