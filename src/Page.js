import React from "react";
import Header from "./Header";
import { Parallaxed, CenterContainer } from "./StyledComponents";
import Footer from "./Footer";
import MetaTags from "./MetaTags";

export default ({ children, ...rest }) => (
  <React.Fragment>
    <MetaTags {...rest} />
    <Header />
    <Parallaxed>{children}</Parallaxed>
    <CenterContainer width={1200}>
      <Footer />
    </CenterContainer>
  </React.Fragment>
);
