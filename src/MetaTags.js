import React from "react";
import Head from "next/head";

export default ({ title, url, image, description }) => (
  <Head>
    <title>{title}</title>
    <link
      rel="alternate"
      type="application/rss+xml"
      title="Gal Schlezinger's thoughts"
      href="/rss.xml"
    />
    <meta property="og:title" content={title} />
    <meta property="og:url" content={url} />
    <meta property="og:image" content={image} />
    <meta property="og:description" content={description} />
    <link rel="shortcut icon" href="/static/favicon.png" />
    <meta charSet="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta charset="utf-8" content="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@galstar" />
    <meta name="twitter:creator" content="@galstar" />
    <link
      href="https://fonts.googleapis.com/css?family=Assistant|David+Libre"
      rel="stylesheet"
    />
  </Head>
);
