import React from "react";
import Link from "next/link";

export const EitherLink = ({ href, insideLink, ...rest }) => {
  return insideLink ? (
    <Link href={href}>
      <a {...rest} />
    </Link>
  ) : (
    <a href={href} {...rest} />
  );
};

export default EitherLink;
