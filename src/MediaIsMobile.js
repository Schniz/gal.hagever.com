import React from "react";
import ReactMedia from "react-media";

export default ({ children }) => (
  <ReactMedia query="(max-width: 500px)" defaultMatches={false}>
    {children}
  </ReactMedia>
);
