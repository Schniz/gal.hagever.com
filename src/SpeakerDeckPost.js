import React from "react";
import moment from "moment";
import { Article, PostFooter, Link, PostTitle } from "./PostDesign";
import styled from "styled-components";
import MediaIsMobile from "./MediaIsMobile";

const Media = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 500px) {
    display: block;
  }
`;

const MediaMinimized = styled.div`
  img {
    width: 14em;
  }

  @media (max-width: 500px) {
    margin-top: 0.5em;
    text-align: center;

    img {
      width: 100%;
    }
  }
`;

const MediaSpread = styled.div`
  ${p => !p.mobile && "margin-left: 1em;"} flex: 1;
`;

export const joinCategories = categories =>
  categories.length === 1
    ? categories[0]
    : categories.slice(0, categories.length - 1).join(", ") +
      " and " +
      categories[categories.length - 1];

export default ({
  title,
  posted_at,
  link,
  thumbnail,
  description,
  categories,
  place
}) => (
  <Link href={link}>
    <Article>
      <PostTitle>{title}</PostTitle>
      <MediaIsMobile>
        {isMobile => (
          <Media>
            <MediaMinimized mobile={isMobile}>
              <img src={thumbnail} alt={[title, description].join("\n")} />
            </MediaMinimized>
            <MediaSpread mobile={isMobile}>
              <p>{description}</p>
              <PostFooter>
                A talk about {joinCategories(categories)}.
              </PostFooter>
              <PostFooter>
                Presented on {moment(posted_at).format("MMMM Do YYYY")} in{" "}
                {place}
              </PostFooter>
            </MediaSpread>
          </Media>
        )}
      </MediaIsMobile>
    </Article>
  </Link>
);
