import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";

const { posts } = require("../contents");
const setDates = xs =>
  xs.map(x => ({ ...x, posted_at: new Date(x.posted_at) }));
const descending = prop => (a, b) => b[prop] - a[prop];
const postsWithDates = setDates(posts).sort(descending("posted_at"));

ReactDOM.render(
  <App posts={postsWithDates} />,
  document.getElementById("root")
);
