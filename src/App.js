import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import Measure from "react-measure";
import WindowScroll from "./WindowScroll";
import { BrowserRouter } from "react-router-dom";
import PostListPage from "./PostListPage";
import { Parallaxed, CenterContainer } from "./StyledComponents";
import { Switch, Route } from "react-router";
import HashlamaPost from "./HashlamaPost";
import DocumentTitle from "react-document-title";

export default class App extends React.Component {
  state = { headerSize: 0 };
  setSize = ({ bottom: headerSize }) => this.setState({ headerSize });

  render() {
    return (
      <BrowserRouter>
        <div>
          <DocumentTitle title="Gal Schlezinger" />
          <WindowScroll onScroll={this.onScroll} />
          <Measure onMeasure={this.setSize}>
            <Header />
          </Measure>
          <Parallaxed topMargin={this.state.headerSize}>
            <Switch>
              <Route
                path="/posts/command-in-officer-training-course"
                component={HashlamaPost}
              />
              <Route render={() => <PostListPage posts={this.props.posts} />} />
            </Switch>
            <CenterContainer width={1200}>
              <Footer />
            </CenterContainer>
          </Parallaxed>
        </div>
      </BrowserRouter>
    );
  }
}
