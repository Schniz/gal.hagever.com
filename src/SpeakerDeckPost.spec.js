import { joinCategories } from "./SpeakerDeckPost";

let mockIsMobile = false;
jest.mock("./MediaIsMobile", () => null);

describe("joinCategories", () => {
  it("should join categories", () => {
    expect(joinCategories(["a"])).toEqual("a");
    expect(joinCategories(["a", "b"])).toEqual("a and b");
    expect(joinCategories(["a", "b", "c"])).toEqual("a, b and c");
  });
});

it("renders correctly", () => {});
