import React from "react";
import BlogPost from "./BlogPost";
import SpeakerDeckPost from "./SpeakerDeckPost";

const componentTypes = {
  blog: BlogPost,
  speakerdeck: SpeakerDeckPost
};

export const Post = post => {
  const Component = componentTypes[post.type];
  return <Component {...post} />;
};

export default Post;
