import React from "react";

export default class WindowScroll extends React.Component {
  handleScroll = e => {
    this.props.onScroll && this.props.onScroll(e);
  };

  componentDidMount() {
    document.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    document.removeEventListener("scroll", this.handleScroll);
  }

  render() {
    if (!this.props.children) return null;
    return React.Children.only(this.props.children);
  }
}
