// eslint-disable-next-line import/no-webpack-loader-syntax
import rawPost from "!raw-loader!../posts/command-in-officer-training-course.md";
import React from "react";
import styled from "styled-components";
import { CenterContainer } from "./StyledComponents";
import compileMarkdown from "./compileMarkdown";
import ScrollToTopOnMount from "./ScrollToTopOnMount";
import { RED } from "./PostDesign";
import MediaIsMobile from "./MediaIsMobile";
import EitherLink from "./EitherLink";

const SANS_SERIF_FONT = "Assistant";
const SERIF_FONT = "David Libre";

const prefixText = (prefix, Component) => ({ children, className }) => (
  <Component className={className}>
    {prefix} {children}
  </Component>
);

const Paragraph = styled.p`
  line-height: 1.4;
  font-family: ${SERIF_FONT};
  font-size: 1.1em;
`;

const BigHeading = styled(prefixText("#", "h1"))`
  color: ${RED};
  font-size: 2em;
  font-family: ${SANS_SERIF_FONT};
`;

const MediumHeading = styled.h2`
  padding: 1em;
  font-weight: 100;
  text-align: center;
  font-family: ${SANS_SERIF_FONT};
  color: rgba(0, 0, 0, 0.6);
`;

const SmallHeading = styled(prefixText("##", "h3"))`
  font-family: ${SANS_SERIF_FONT};
`;

const Blockquote = styled.blockquote`
  margin-top: -1.8em;
  font-size: 0.7em;
  color: ${RED};
  font-family: ${SANS_SERIF_FONT};
`;

const OrderedList = styled.ol`
  padding-right: ${p => (p.isMobile ? 1.5 : 5)}em;
  line-height: 2;

  & li {
    font-family: ${SERIF_FONT};
    transition: 0.2s ease all;
    font-size: 1.3em;
  }

  ${p =>
    !p.isMobile &&
    `
    &:hover li {
      opacity: 0.5;
    }

    &:hover li:hover {
      opacity: 1;
    }
  `};
`;

const ResponsiveOrderedList = ({ children }) => (
  <MediaIsMobile>
    {isMobile => <OrderedList isMobile={isMobile}>{children}</OrderedList>}
  </MediaIsMobile>
);

const elements = {
  p: Paragraph,
  h1: BigHeading,
  h2: MediumHeading,
  h3: SmallHeading,
  ol: ResponsiveOrderedList,
  blockquote: Blockquote
};

const RightToLeftArticle = styled.article`
  direction: rtl;
  line-height: 1.4;
  font-size: 1.5em;
  padding: 0.5em;
`;

export default () => (
  <ScrollToTopOnMount>
    <CenterContainer width={1200}>
      <RightToLeftArticle>
        {compileMarkdown(elements, rawPost)}
      </RightToLeftArticle>

      <EitherLink insideLink href="/">
        חזור
      </EitherLink>
    </CenterContainer>
  </ScrollToTopOnMount>
);
