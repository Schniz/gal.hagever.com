import styled from "styled-components";
import EitherLink from "./EitherLink";

export const RED = "#cf3232";
export const BRIGHT_RED = "#c79e9e";

export const Link = styled(EitherLink)`
  text-decoration: none;
  color: #333;

  & h1 {
    color: #cf3232;
    margin: 0;
    padding: 0;
  }

  & p {
    border-left: 0.4em solid #c79e9e;
    padding: 0.4em;
  }
`;

export const Article = styled.article`
  padding: 2em 0;
  line-height: 1.4;
  transition: all 0.2s ease;

  &:hover {
    opacity: 1 !important;
  }
`;

export const PostTitle = styled.h1`
  font-family: Helvetica Neue, Helvetica, Arial;
`;

export const PostFooter = styled.small`
  font-family: Helvetica Neue, Helvetica, Arial;
  display: block;
`;
