import React from "react";
import moment from "moment";
import { Article, PostTitle, PostFooter, Link } from "./PostDesign";

export default ({ title, inside_link, posted_at, description, link }) => (
  <Link insideLink={inside_link} href={link}>
    <Article>
      <PostTitle dir="auto">{title}</PostTitle>
      <p dir="auto">{description}</p>
      <PostFooter dir="auto">
        Published on {moment(posted_at).format("MMMM Do YYYY")}
      </PostFooter>
    </Article>
  </Link>
);
